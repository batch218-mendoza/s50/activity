import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}) {

    const{title, content, destination, label} = data;
//REFACTOR BANNER FOR ERROR PAGE
return (
    <Row>
        <Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>
            <Button variant="primary" as={Link} to={destination}>{label}</Button>
            {/*<Link to={destination} >{label}</Link>*/}
        </Col>
    </Row>
    )
}






import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
   <React.StrictMode>
     <App />
   </React.StrictMode>
);

// JSX - javascript XML
// allows us to write HTML in React

// const name = "Jan Jonell Yao Mendoza";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Jan Jonell',
//   lastName: 'Mendoza'
// };

// function formatName(user){
//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>

/*root.render(element);*/

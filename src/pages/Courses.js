import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

import {useState, useEffect} from 'react';

export default function Courses() {

	//state that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// The "map" method loops through the individual course object in our array and returns a component for each course
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData aray using the "courseProp" 
	// const courses = coursesData.map(course => {
	// 	return (

	// 			<CourseCard key={course.id} course={course} />

	// 		);
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`,)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setCourses(data.map(course => {
				return (
				<CourseCard key={course.id} course={course}/>
				)
			}))
		})
	}, []);


	return(
			<>
			{courses}
			</>
		)
}
import { Link } from 'react-router-dom';

import Banner from '../components/Banner';



// export default function PageNotFound() {


// 	return (
// 		<div>
// 				<h1>Page Not Found</h1>
//               <Link to="/">Go back to homepage</Link>
            
//            </div>
// 		)

// };


export default function Error(){

const data = {
	title: "Error 404 - page not found.",
	content: "The page you are looking for cannot be found",
	destination: "/",
	label: "back to home"
}
return  (
		<Banner data={data} />	

	)


	}